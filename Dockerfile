ARG R_VERSION
FROM r-base:${R_VERSION}

COPY install_packages.R /tmp/

RUN apt-get update \
    && apt-get install -y libxml2-dev libcurl4-openssl-dev libssl-dev libgsl-dev libhdf5-dev \
    && Rscript /tmp/install_packages.R

CMD ["R"]
